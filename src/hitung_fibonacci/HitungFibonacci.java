package hitung_fibonacci;

import java.util.Scanner;
import java.math.BigInteger;



public class HitungFibonacci {

    public static void main(String[] args) {
        String identitas = "Dhimas Wahyu Prayogi / XR7";
        tampilJudul(identitas);
    }

    private static void tampilJudul(String identitas) {
        System.out.println("Identitas " + identitas);
        System.out.println("Hitung FIbbonaci");
    }

    private static int tampilInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Bilangan ke-:");
        int n = scanner.nextInt();
        return n;
              
    }
    private static BigInteger fibo(int n) {
        BigInteger[] hasil = new BigInteger[n];
        
        hasil[0] = BigInteger.ONE;
        hasil[1] = BigInteger.ONE;
        
        for (int i = 2; i < n; i++) {
            hasil[i] = hasil[i-1].add(hasil[i-2]);
        }
        return hasil[n-1];
    }
    
   

    }


